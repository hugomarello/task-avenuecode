package assignment.avenuecode.task;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/** 
 * Setting up jersey and spring integration, for JAX-RS use
 * @author hugo
 *
 */
@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        packages(getClass().getPackage().getName() + ".apis");
    }
}