package assignment.avenuecode.task.apis;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import assignment.avenuecode.task.persistence.entities.BaseEntity;
import assignment.avenuecode.task.services.BaseService;

public abstract class BaseApi <T extends BaseEntity> {

	public abstract BaseService<T> getService();
	public static final String CREATED = "Object created!";
	public static final String DELETED = "Object deleted!";
	public static final String UPDATED = "Object updated!";
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchAll() {
		List<T> result = getService().fetchAll();
		return Response.ok().entity(result).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchById(@PathParam("id") Long id) {
		T result = getService().fetchById(id);
		return Response.ok().entity(result).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		getService().delete(id);
		return Response.ok().entity(BaseApi.DELETED).build();
    }
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") Long id, T newValue) {
		//in case the id is not set, or wrong
		newValue.setId(id);
		getService().update(newValue);
		return Response.ok().entity(BaseApi.UPDATED).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(T value) {
		getService().create(value);
		return Response.ok().entity(BaseApi.CREATED).build();
	}
	
}
