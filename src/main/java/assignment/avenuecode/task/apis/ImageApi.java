package assignment.avenuecode.task.apis;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;

import assignment.avenuecode.task.persistence.entities.Image;
import assignment.avenuecode.task.services.BaseService;
import assignment.avenuecode.task.services.ImageService;

@Path("image")
public class ImageApi extends BaseApi<Image> {
	
	@Autowired
	private ImageService service;
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	@Override
	public BaseService<Image> getService() {
		return service;
	}

}
