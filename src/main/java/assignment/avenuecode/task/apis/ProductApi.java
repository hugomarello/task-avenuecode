package assignment.avenuecode.task.apis;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import assignment.avenuecode.task.persistence.entities.Image;
import assignment.avenuecode.task.persistence.entities.Product;
import assignment.avenuecode.task.services.BaseService;
import assignment.avenuecode.task.services.ProductService;

@Path("product")
public class ProductApi extends BaseApi<Product> {
	
	@Autowired
	private ProductService service;
	
	@QueryParam("showRelationship")
	private String showRelationship;
	
	@Override
	public BaseService<Product> getService() {
		return service;
	}
	
	@Override
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchAll() {
		return Response.ok().entity(service.fetchAll(new Boolean(showRelationship))).build();
	}
	
	@Override
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchById(@PathParam("id") Long id) {
		return Response.ok().entity(service.fetch(id, new Boolean(showRelationship))).build();
	}
	
	@GET
	@Path("/{id}/image")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Image> fetchImages(@PathParam("id") Long id) {
		return service.fetchImages(id);
	}
	
	@GET
	@Path("/{id}/children")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchChildren(@PathParam("id") Long id) {
		return Response.ok().entity(service.fetchChildren(id)).build();
	}
}
