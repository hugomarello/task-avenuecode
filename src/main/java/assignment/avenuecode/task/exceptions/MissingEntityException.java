package assignment.avenuecode.task.exceptions;

import assignment.avenuecode.task.persistence.entities.BaseEntity;

public class MissingEntityException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public MissingEntityException(Long id, Class<? extends BaseEntity> clazz){
        super("Couldn't find entity. For entity: "+clazz.getSimpleName()+" with id: "+id);
    }
    public MissingEntityException(Class<? extends BaseEntity> clazz){
        super("Couldn't find entity. For entity: "+clazz.getSimpleName()+" with unknown id");
    }
}
