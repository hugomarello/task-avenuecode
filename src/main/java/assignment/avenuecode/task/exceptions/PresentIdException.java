package assignment.avenuecode.task.exceptions;

import assignment.avenuecode.task.persistence.entities.BaseEntity;

public class PresentIdException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public PresentIdException(Long id, Class<? extends BaseEntity> clazz){
        super("New entities can't be persisted with an 'id', it will be generated automatically. For entity: "+clazz.getSimpleName()+" with id: "+id);
    }
}
