package assignment.avenuecode.task.persistence.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import assignment.avenuecode.task.exceptions.MissingEntityException;
import assignment.avenuecode.task.exceptions.PresentIdException;
import assignment.avenuecode.task.persistence.entities.BaseEntity;

@Transactional
public abstract class BaseDao<T extends BaseEntity> {
	@Autowired
	private EntityManager em;
	
	/**
	 * Used to maintain the Generic Type during execution
	 */
	private Class<T> clazz;

	public BaseDao (Class<T> clazz){
		this.clazz = clazz;
	}

	public T find(Long id) {
		return (T) em.find(clazz, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll () {
		return em.createQuery("from " + clazz.getName()).getResultList();
	}

	public void save(final T entity) {
	    if(entity.getId() != null){
	        throw new PresentIdException(entity.getId(), clazz);
	    }
		em.persist(entity);
	}

	public T update(final T entity) {
	    if(find(entity.getId()) != null){
	        return (T) em.merge(entity);
	    }
	    throw new MissingEntityException(entity.getId(), clazz);
	}

	public void delete(final T entity) {
	    if(entity != null){
	        em.remove(entity);
	    } else{
	        throw new MissingEntityException(clazz);
	    }
	}

	public void delete(Long id) {
		T entity = find(id);
		if(entity == null){
		    throw new MissingEntityException(id, clazz);
		}
		delete(entity);
	}

	protected EntityManager getEntityManager() {
		return em;
	}
}
