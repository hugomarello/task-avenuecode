package assignment.avenuecode.task.persistence.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import assignment.avenuecode.task.persistence.entities.Image;

@Repository
public class ImageDao extends BaseDao<Image> {

	ImageDao() {
		super(Image.class);
	}

	@SuppressWarnings("unchecked")
	public List<Image> fetchByProduct(Long id) {
		Query query = getEntityManager().createQuery("SELECT p.images FROM Product p WHERE p.id=:id");
		query.setParameter("id", id);
		return query.getResultList();
	}

}
