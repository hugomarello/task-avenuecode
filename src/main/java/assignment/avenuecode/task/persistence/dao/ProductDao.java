package assignment.avenuecode.task.persistence.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import assignment.avenuecode.task.exceptions.PresentIdException;
import assignment.avenuecode.task.persistence.entities.Image;
import assignment.avenuecode.task.persistence.entities.Product;

@Repository
public class ProductDao extends BaseDao<Product> {

	ProductDao() {
		super(Product.class);
	}

	@SuppressWarnings("unchecked")
	public List<Product> fetchAllWithRelationship() {
		return getEntityManager().createQuery("SELECT distinct p FROM Product p LEFT JOIN FETCH p.images LEFT JOIN FETCH p.parent" ).getResultList();
	}
	
	@Override
	public void save(Product product) {
		saveImageRelationships(product);
		
		saveParentRelationship(product);
		
		super.save(product);
	}

    private void saveParentRelationship(Product product) {
        Product parent = product.getParent();
        
		if( parent != null && parent.getId() != null) {
			Product savedParent = getEntityManager().find(Product.class, product.getParent().getId());
			
			if(savedParent != null) {
				//product with this id exists, use original
				product.setParent(savedParent);
			} else {
			    // it is a new product, cascade will persist it
			    if(parent.getId() != null){
			        throw new PresentIdException(parent.getId(), Product.class);
			    }
			}
		}
    }

    private void saveImageRelationships(Product product) {
        List<Image> images = product.getImages();
		
		if(images != null) {
			List<Image> newImages = new ArrayList<Image>();
			
			for(Image image:images) {
				Image savedImage = null;
				
				if(image.getId() != null) {
					savedImage = getEntityManager().find(Image.class, image.getId());					
				}

				if(savedImage != null) {
					//image was saved before, better not change its fields
					savedImage.setProduct(product);
					newImages.add(savedImage);
				}else {
				    if(image.getId() != null){
				        throw new PresentIdException(image.getId(), Image.class);
				    }
					//its a new image, its ok to save it
					image.setProduct(product);
					newImages.add(image);
				}
			}
			product.setImages(newImages);
		}
    }
	
	@Override
	public Product update(Product product) {
		Product savedProduct = getEntityManager().find(Product.class, product.getId());
		List<Image> newImagesList = product.getImages();
		Map<Long, Image> newImagesMap = new HashMap<Long, Image>();
				
		if(newImagesList != null) {
			newImagesMap = newImagesList.stream().collect(Collectors.toMap(Image::getId, item -> item));
			
			//setting the reference in the new images
			newImagesList.stream().forEach( image -> image.setProduct(product));
		}

		
		//old images, that were removed from the relation must be set to null
		if(savedProduct != null){
    		for(Image image : savedProduct.getImages()) {
    			if( !newImagesMap.containsKey(image.getId())) {
    				image.setProduct(null);
    				getEntityManager().merge(image);
    			}
    		}
		}

		return (Product) getEntityManager().merge(product);
	}

	public Product fetchWithRelationship(Long id) {
		Query query = getEntityManager().createQuery("SELECT p FROM Product p LEFT JOIN FETCH p.images LEFT JOIN FETCH p.parent WHERE p.id=:id");
		query.setParameter("id", id);
		return (Product) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public List<Product> fetchChildren(Long id) {
		Query query = getEntityManager().createQuery("SELECT p FROM Product p "
				+ "LEFT JOIN FETCH p.images "
				+ "LEFT JOIN FETCH p.parent parent "
				+ "WHERE parent.id=:id");
		query.setParameter("id", id);
		
		return query.getResultList();
	}

}
