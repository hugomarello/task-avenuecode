package assignment.avenuecode.task.persistence.entities;

import java.io.Serializable;

public interface BaseEntity extends Serializable {

	public Long getId();
	public void setId(Long id);
}
