package assignment.avenuecode.task.persistence.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

import assignment.avenuecode.task.utils.Views;

@Entity
public class Product implements BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@JsonView(Views.EagerOnly.class)
	private Long id;
	
	@Column
	@JsonView(Views.EagerOnly.class)
	private String name;

	@OneToMany(fetch = FetchType.LAZY, mappedBy="product", cascade= {CascadeType.MERGE, CascadeType.PERSIST})
	@JsonView(Views.All.class)
	private List<Image> images;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade= {CascadeType.MERGE, CascadeType.PERSIST})
	@JsonView(Views.All.class)
    private Product parent;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}
	
}
