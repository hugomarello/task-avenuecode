package assignment.avenuecode.task.services;

import java.util.List;

import assignment.avenuecode.task.persistence.dao.BaseDao;
import assignment.avenuecode.task.persistence.entities.BaseEntity;

public abstract class BaseService < T extends BaseEntity > {

	public abstract BaseDao<T> getDao();
	
	public List<T> fetchAll(){
		return getDao().findAll();
	}
	
	public T fetchById(Long id) {
		return getDao().find(id);
	}
	
	public void delete(Long id) {
		getDao().delete(id);
	}
	
	public void update(T newValue) {
		getDao().update(newValue);
	}
	
	public void create(T value) {
		getDao().save(value);
	}
}
