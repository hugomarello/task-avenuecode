package assignment.avenuecode.task.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import assignment.avenuecode.task.persistence.dao.BaseDao;
import assignment.avenuecode.task.persistence.dao.ImageDao;
import assignment.avenuecode.task.persistence.entities.Image;

@Service
@Transactional
public class ImageService extends BaseService<Image> {

	@Autowired
	private ImageDao dao;

	@Override
	public BaseDao<Image> getDao() {
		return dao;
	}

	public List<Image> fetchByProduct(Long id) {
		return dao.fetchByProduct(id);
	}

}
