package assignment.avenuecode.task.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import assignment.avenuecode.task.persistence.dao.BaseDao;
import assignment.avenuecode.task.persistence.dao.ProductDao;
import assignment.avenuecode.task.persistence.entities.Image;
import assignment.avenuecode.task.persistence.entities.Product;
import assignment.avenuecode.task.utils.JsonCustomMapper;
import assignment.avenuecode.task.utils.Views;

@Service
@Transactional
public class ProductService extends BaseService<Product> {

	@Autowired
	private ProductDao dao;
	
	@Autowired
	private ImageService imageService;

	@Override
	public BaseDao<Product> getDao() {
		return dao;
	}
	
	public String fetch(Long id, Boolean showRelationship) {
		Product product;
		
		if(showRelationship) {
			product = dao.fetchWithRelationship(id);
			return JsonCustomMapper.mapJsonWithView(product, Views.All.class);
		}else {
			product = super.fetchById(id);
			return JsonCustomMapper.mapJsonWithView(product, Views.EagerOnly.class);
		}
	}

	public String fetchAll(Boolean showRelationship){
		List<Product> products;
		
		if(showRelationship) {
			products = dao.fetchAllWithRelationship();
			return JsonCustomMapper.mapJsonWithView(products, Views.All.class);
		}else {
			products = super.fetchAll();
			return JsonCustomMapper.mapJsonWithView(products, Views.EagerOnly.class);
		}
	}

	public List<Image> fetchImages(Long id) {
		return imageService.fetchByProduct(id);
	}

	public String fetchChildren(Long id) {
		List<Product> products = dao.fetchChildren(id);

		return JsonCustomMapper.mapJsonWithView(products, Views.EagerOnly.class);

	}

}
