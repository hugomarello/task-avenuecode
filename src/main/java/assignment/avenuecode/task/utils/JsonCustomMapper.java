package assignment.avenuecode.task.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonCustomMapper {

    static public String mapJsonWithView(Object value, Class<?> jsonView){
        ObjectMapper mapper = new ObjectMapper();
        
        try {
            return mapper
                    .writerWithView(jsonView)
                    .writeValueAsString(value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
