package assignment.avenuecode.task.api;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import assignment.avenuecode.task.apis.BaseApi;
import assignment.avenuecode.task.persistence.dao.BaseDao;
import assignment.avenuecode.task.persistence.entities.Image;
import assignment.avenuecode.task.services.BaseService;

@RunWith(MockitoJUnitRunner.class)
public class BaseApiTest {
    
    private int httpOK = 200;
    private Long mockId = 0L;
    
    @Spy
    private BaseApi<Image> apiToTest;
    @Spy
    private BaseService<Image> mockService;
    
    class MockDao extends BaseDao<Image>{
        protected MockDao(){
            super(Image.class);
        }
        public Image find(Long id) {
            return null;
        }

        public List<Image> findAll () {
            return null;
        }

        public void save(Image entity) {
        }

        public Image update(Image entity) {
            return null;
        }

        public void delete(Image entity) {
            
        }

        public void delete(Long id) {

        }
    }
    
    private MockDao mockDao;
    
    @Before
    public void init(){
        mockDao = new MockDao();
        
        Mockito.when(mockService.getDao()).thenReturn(mockDao);
        Mockito.when(apiToTest.getService()).thenReturn(mockService);
    }
    
    @Test
    public void mustFetchAll(){
        Response result = apiToTest.fetchAll();
        Mockito.verify(mockService).fetchAll();
        assertEquals(httpOK, result.getStatus());
    }
    
    @Test
    public void mustFetchById(){
        Response result = apiToTest.fetchById(mockId);
        Mockito.verify(mockService).fetchById(mockId);
        assertEquals(httpOK, result.getStatus());
    }
    
    @Test
    public void mustDelete(){
        Response result = apiToTest.delete(mockId);
        Mockito.verify(mockService).delete(mockId);
        assertEquals(httpOK, result.getStatus());
        assertEquals(BaseApi.DELETED, result.getEntity());
    }
    
    @Test
    public void mustUpdate(){
        Image image = new Image();
        
        Response result = apiToTest.update(mockId, image);
        Mockito.verify(mockService).update(image);
        assertEquals(httpOK, result.getStatus());
        assertEquals(BaseApi.UPDATED, result.getEntity());
    }
    
    @Test
    public void mustCreate(){
        Image image = new Image();
        
        Response result = apiToTest.create(image);
        Mockito.verify(mockService).create(image);
        assertEquals(httpOK, result.getStatus());
        assertEquals(BaseApi.CREATED, result.getEntity());
    }
}
