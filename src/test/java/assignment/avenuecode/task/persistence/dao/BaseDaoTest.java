package assignment.avenuecode.task.persistence.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import assignment.avenuecode.task.exceptions.MissingEntityException;
import assignment.avenuecode.task.exceptions.PresentIdException;
import assignment.avenuecode.task.persistence.entities.Image;
import assignment.avenuecode.task.test.utils.TestUtilities;

public class BaseDaoTest {
    
    EntityManager mockEntityManager;
    BaseDao<Image> testDao;
    Image mockImage;
    
    @Before
    public void init(){
        mockEntityManager = Mockito.mock(EntityManager.class);
        testDao = new BaseDao<Image>(Image.class){};
        TestUtilities.injectByType(mockEntityManager, EntityManager.class, testDao);
        mockImage = new Image();
        mockImage.setId(0L);
        mockImage.setName("image name");
    }
    
    @Test
    public void mustSave(){
        mockImage.setId(null);
        testDao.save(mockImage);
        Mockito.verify(mockEntityManager).persist(mockImage);
    }
    
    @Test(expected = PresentIdException.class)
    public void mustNotSaveEntitiesWithId(){
        testDao.save(mockImage);
    }
    
    @Test
    public void mustFindEntities(){
        Mockito.when(mockEntityManager.find(Image.class, 0L)).thenReturn(mockImage);
        Mockito.when(mockEntityManager.find(Image.class, 1L)).thenReturn(null);
        Image result = testDao.find(0L);
        assertEquals(mockImage, result);
        result = testDao.find(1L);
        assertNull(result);
    }
    
    @Test
    public void mustUpdate(){
    	Mockito.when(mockEntityManager.find(Image.class, 0L)).thenReturn(mockImage);
        testDao.update(mockImage);
        Mockito.verify(mockEntityManager).merge(mockImage);
    }
    
    @Test(expected = MissingEntityException.class)
    public void mustOnlyUpdateEntitiesWithId(){
    	mockImage.setId(null);
        testDao.update(mockImage);
    }

    @Test
    public void mustDelete(){
        testDao.delete(mockImage);
        Mockito.verify(mockEntityManager).remove(mockImage);
    }
    
    @Test(expected = MissingEntityException.class)
    public void mustFindBeforeDelete(){
    	Mockito.when(mockEntityManager.find(Image.class, 0L)).thenReturn(null);
        testDao.delete(0L);
    }
    
    @Test(expected = MissingEntityException.class)
    public void mustNotDeleteNull(){
        testDao.delete((Image)null);
    }
    
    @Test
    public void mustFindAll() {
    	Query mockQuery = Mockito.mock(Query.class);
    	Mockito.when(mockEntityManager.createQuery("from "+Image.class.getName())).thenReturn(mockQuery);
    	
    	testDao.findAll();
    	Mockito.verify(mockQuery).getResultList();
    }
}
