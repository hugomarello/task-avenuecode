package assignment.avenuecode.task.test.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class TestUtilities {

    /**
     * This method search all classes for a field with the specified type and injects it,
     * only on the first found
     * 
     * @param injectable the object that will be injected
     * @param injectableClazz the class of the injectable
     * @param destination where to be injected
     */
    public static void injectByType(Object injectable, Class<?> injectableClazz, Object destination){
        Class<?> destinationClass = destination.getClass();
        
        for(Class<?> clazz : getAllClasses(destinationClass)){
            for(Field field: clazz.getDeclaredFields()){
                if(field.getType().equals(injectableClazz)){
                    try {
                        field.setAccessible(true);
                        field.set(destination, injectable);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    
    private static List<Class<?>> getAllClasses(Class<?> type) {
        List<Class<?>> listOfClasses = new ArrayList<>();
        for(; type!=null; type=type.getSuperclass()) listOfClasses.add(type);
        return listOfClasses;
    }
}
