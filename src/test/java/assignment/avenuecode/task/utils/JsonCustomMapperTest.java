package assignment.avenuecode.task.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonView;

public class JsonCustomMapperTest {
    
    class MockEntity {
        @JsonView(Views.EagerOnly.class)
        private String eagerString = "eager";
        
        @JsonView(Views.All.class)
        private String lazyString = "lazy";
    }

    private MockEntity testSubject;
    
    @Before
    public void init(){
        testSubject = new MockEntity();
    }
    
    @Test
    public void mustHideLazyAndShowEager(){
        String result = JsonCustomMapper.mapJsonWithView(testSubject, Views.EagerOnly.class);
        assertNotNull(result);
        assertFalse(result.contains("lazy"));
        assertTrue(result.contains("eager"));
    }
    
    @Test
    public void mustShowEagerAndLazy(){
        String result = JsonCustomMapper.mapJsonWithView(testSubject, Views.All.class);
        assertNotNull(result);
        assertTrue(result.contains("lazy"));
        assertTrue(result.contains("eager"));
    }
}